# PROJECT OVERVIEW

<div align="center">
	<hr />
    <p>
        Primary Objective
    </p>
    <hr />
</div>

- Welcome to my submission to the first project of the Udacity-AWS AI Nanodegree course
    - This project involves building a pipeline to process user provided images of dogs and provide an estimate of the particular breed

- Exploring CNN Models
    - Involved the implementation of classification and localization processes
    - Models were pieced together in order to perform tasks in the data processing pipeline
    - Made decisions based on the critique of complexity for chosen archiecture 